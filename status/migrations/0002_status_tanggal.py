# Generated by Django 2.2.6 on 2019-10-31 04:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('status', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='tanggal',
            field=models.DateField(auto_now=True),
        ),
    ]
