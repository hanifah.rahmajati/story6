from django.shortcuts import render, redirect
from . import forms, models

def statuspage(request):
    if request.method == "POST":
        form = forms.StatusForm(request.POST)
        if form.is_valid(): 
            form.save()
            return redirect('status')
    else :
        form = forms.StatusForm()
    statusdata = models.Status.objects.all()
    sta = {
        'form' : form,
        'status_data' : statusdata
    }
    return render(request, 'index.html', sta)

def profilepage(request):
    return render(request, 'profil.html')