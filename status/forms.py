from django import forms
from . import models

class StatusForm(forms.ModelForm):
    class Meta:
        model = models.Status
        fields = ['status']
        
        