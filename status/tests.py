from django.test import TestCase, Client
from . import models, forms, views, urls
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class TestStatusPage(TestCase) :
    def test_url_status_ada(self):
        response = Client().get('/status')
        self.assertEqual(response.status_code, 200)

    def test_html_status(self):
        response = Client().get('/status')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_form_isi_status(self):
        form_data = {
            'status' : 'test'
        }
        status = forms.StatusForm(data = form_data)
        self.assertTrue(status.is_valid())
    
    def test_model_isi_status(self):
        hitung0 = models.Status.objects.all().count()
        statusmodel = models.Status.objects.create(status="test")
        hitung1 = models.Status.objects.all().count()
        self.assertEqual(hitung0, 0)
        self.assertEqual(hitung1, hitung0+1)
        self.assertTrue(isinstance(statusmodel, models.Status))
    
    def test_button_update(self):
        c = Client()
        response = c.get('/status')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("Update Status", content)
    
    def test_invalid_input(self):
        status_form = forms.StatusForm({
            'status': 301 * "X"
        })
        self.assertFalse(status_form.is_valid())
    
    def test_form_validation_for_blank_items(self):
        form = forms.StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    
    def test_status_post_sukses(self):
        response_post = Client().post('/status',{'status':'Halooo'})
        self.assertEqual(response_post.status_code,302)



class TestHomepage(TestCase):
    def test_url_homepage(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_ada_nama_foto(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("Hanifah Rahmajati", content)
        self.assertIn ("<img", content)


class FunctionalTestStatus(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(1)
        super(FunctionalTestStatus,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTestStatus, self).tearDown()
    
    def test_status(self):
        self.browser.get('http://127.0.0.1:8000/status')
        self.assertIn('Status Keadaan Saya', self.browser.title)
        text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Halo, Apa Kabar?', text)
        inputstatus = self.browser.find_element_by_id('id_status')
        time.sleep(1)
        inputstatus.send_keys('Halo')
        time.sleep(1)
        inputstatus.send_keys(Keys.ENTER)
        time.sleep(5)
        self.assertIn('Halo', self.browser.page_source)
    
    def test_accordion(self):
        self.browser.get('http://127.0.0.1:8000/')
        accordion1 = self.browser.find_element_by_id('headaktiv')
        time.sleep(1)
        self.assertIn("Aktivitas",accordion1.text)
        accordion2 = self.browser.find_element_by_id('headpengalaman')
        time.sleep(1)
        self.assertIn("Pengalaman Kepanitiaan",accordion2.text)
        time.sleep(1)
        accordion2.send_keys(Keys.ENTER)
        time.sleep(1)
        listpengalaman = self.browser.find_elements_by_tag_name('li')
        self.assertIn("Anggota Acara CSL 2019", listpengalaman[0].text)
    
    def test_change_homepage(self):
        self.browser.get('http://127.0.0.1:8000/')
        toggle = self.browser.find_element_by_id('themes')
        time.sleep(1)
        body = self.browser.find_element_by_css_selector('body')        
        heading = self.browser.find_element_by_css_selector('.panel-heading')
        time.sleep(1)
        color = body.value_of_css_property('background-color')
        colorhead = heading.value_of_css_property('background')
        time.sleep(1)
        self.assertEqual("rgba(245, 245, 245, 1)", color)
        self.assertEqual("rgb(238, 174, 202) none repeat scroll 0% 0% / auto padding-box border-box", colorhead)
        time.sleep(1)
        toggle.click()
        time.sleep(1)
        color2 = body.value_of_css_property('background-color')
        colorhead2 = heading.value_of_css_property('background')
        self.assertEqual("rgba(0, 0, 0, 1)", color2)
        self.assertEqual("rgb(0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box", colorhead2)

    def test_change_statuspage(self):
        self.browser.get('http://127.0.0.1:8000/status')
        toggle = self.browser.find_element_by_id('themes')
        time.sleep(1)
        body = self.browser.find_element_by_css_selector('body')
        form = self.browser.find_element_by_css_selector('#id_status')
        time.sleep(1)
        color = body.value_of_css_property('background-color')
        colorform = form.value_of_css_property('border')
        time.sleep(1)
        self.assertEqual("rgba(245, 245, 245, 1)", color)
        self.assertEqual("2px solid rgb(0, 0, 0)", colorform)
        time.sleep(1)
        toggle.click()
        time.sleep(1)
        color2 = body.value_of_css_property('background-color')
        colorform2 = form.value_of_css_property('border')
        self.assertEqual("rgba(0, 0, 0, 1)", color2)
        self.assertEqual("2px solid rgb(255, 255, 255)", colorform2)

